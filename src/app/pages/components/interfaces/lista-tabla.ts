import { Obligaciones } from "./obligaciones.interface";

export interface ListaTabla {
    obligaLinea: number;
    archivos: Obligaciones[]
}
