import {ListasExpe} from "./lista-expe";

export interface ListaExpediente {
  result: boolean;
  lista_expedientes: ListasExpe[];
}
