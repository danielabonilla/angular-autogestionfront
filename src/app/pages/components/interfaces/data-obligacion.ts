export interface DataObligacion {
  expecter: number;
  sequenceThirdParty: number;
  niLinea: number;
}
