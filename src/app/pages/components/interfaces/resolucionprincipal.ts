import {Resolucion} from "./resolucion.interface";

export interface resolucionPrincipal {
  result: boolean;
  resolution: Resolucion[];
}
