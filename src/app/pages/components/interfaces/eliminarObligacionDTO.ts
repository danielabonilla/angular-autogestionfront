export interface EliminarObligacionDTO {
    result: boolean;
    niLinea: number;
    voError: string;
}