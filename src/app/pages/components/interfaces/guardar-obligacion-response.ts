export interface GuardarObligacionResponse {
    result: boolean;
    line: string;
    mensaje: string;
}
