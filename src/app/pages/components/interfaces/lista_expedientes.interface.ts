import {ListaExpedienteArr} from "./lista_expediente-arr.interface";

export interface ListaExpedientes {
  result: boolean;
  lista_expedientes: ListaExpedienteArr[];
}
