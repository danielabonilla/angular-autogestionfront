import {Archivos} from './archivos';

export interface ResultConsultaArchivo {
  result: boolean;
  archivos: Archivos[];
}
