export interface Archivos {
  resul: Boolean;
  obliga_linea: number;
  expediente: string;
  obliga_descripcion: string;
  obliga_obs: string;
  obliga_fecha_creacion: string;
  archivo_nro: number;
  archivo_nombre: string;
  fecha_ingresa_archivo: string;
  doc_sec_resol: string;
  radicado_resolucion: string;
  obligaLinea: number;
  obligaDescripcion: String;
  obligaObservacion: String;
  obligaFechaCreacion: Date;
  docSecResol: number;
  radicadoResolucion: string;
  archivoNro: number;
  niNroArchivo: number;
  archivoNombre: string;
  archivo: null;
  fechaIngresoArchivo: Date;
  fechaFoto: Date;
  coorX: number;
  coorY: number;
  viMotivoParcial:string;
  comoTermina:string;
}
