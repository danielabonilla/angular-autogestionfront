export interface Resolucion {
  expe_sec: number;
  radicado: string;
  fecha: string;
  descriptor: string;
  link_descarga: string;
}
