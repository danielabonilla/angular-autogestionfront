export interface ResultRadicado {
  noSecDocCoe: number;
  voRadicadoCoe: string;
  doRadicado: string;
  voRutaCoe: string;
  voError: string;
}
